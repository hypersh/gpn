﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class hyperlink : MonoBehaviour
{
    public BrowserDetails browser;
    public Text myText;
    public Image myLine;
    public Color myColor_normal;
    public Color myColor_hover;
    public string myUrl;
    bool desktop;
    //i ma sencealry hate GPN management

    // Start is called before the first frame update
    void Start()
    {
        desktop = browser.trueDesktop;
    }

    // Update is called once per frame
    public void onhover()
    {
        if (desktop == true)
        {
            myLine.color = myColor_hover;
            myText.color = myColor_hover;
            browser.CursorHover();
        }
    }

    public void onUNhover()
    {
        if (desktop == true)
        {
            myLine.color = myColor_normal;
            myText.color = myColor_normal;
            browser.CursorNormal();
        }
    }

    public void onClick()
    {
        if (desktop == true)
        {
            myLine.color = myColor_normal;
            myText.color = myColor_normal;
            browser.CursorNormal();
            openWindow(myUrl);
        }
        if (desktop == false)
        {
            Application.OpenURL(myUrl);
        }
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);

}
