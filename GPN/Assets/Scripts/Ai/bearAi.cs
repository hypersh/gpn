﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bearAi : MonoBehaviour
{
    public bool Selected = false;

    public Animator myAnimator;
    string triggerName = "LookingAt";


    // Start is called before the first frame update
    void Start()
    {
        myAnimator = this.gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (Selected == true)
        {
            myAnimator.SetTrigger(triggerName);
        }

        if (Selected == false)
        {
            myAnimator.ResetTrigger(triggerName);
        }
    }

}
