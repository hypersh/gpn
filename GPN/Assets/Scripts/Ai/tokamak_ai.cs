﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tokamak_ai : MonoBehaviour
{
    public float plasma_MatoffsetSpeed;
    public float breating;
    public Material plasmaMat;
    float offseter;

    public Transform circle_1;
    Vector3 c1_initscale;
    public Transform circle_2;
    Vector3 c2_initscale;
    Vector3 scale;
    float scaleFloat;
    // Start is called before the first frame update
    void Start()
    {
        c1_initscale = circle_1.localScale;
        c2_initscale = circle_2.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        offseter = plasma_MatoffsetSpeed * Time.time;
        plasmaMat.SetTextureOffset("_MainTex", new Vector2(offseter, 0.58f));

        scaleFloat = Mathf.PingPong(Time.time, breating);

        scale = new Vector3(scaleFloat + 1, scaleFloat + 1, scaleFloat + 1);
        circle_1.localScale = scale;
        circle_2.localScale = scale;
    }
}
