﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dronesAI : MonoBehaviour
{
    public float coolerRotateSpeed;
    public GameObject myCooler_1;
    public GameObject myCooler_2;
    public GameObject myCooler_3;
    public GameObject myCooler_4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        myCooler_1.transform.Rotate(0, coolerRotateSpeed, 0);
        myCooler_2.transform.Rotate(0, -coolerRotateSpeed, 0);
        myCooler_3.transform.Rotate(0, coolerRotateSpeed, 0);
        myCooler_4.transform.Rotate(0, -coolerRotateSpeed, 0);
    }
}
