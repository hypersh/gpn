﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdAi : MonoBehaviour
{
    public Animator myAnimator;
    string animationName = "birdLoop";

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = this.gameObject.GetComponent<Animator>();


        if (myAnimator != null)
        {

            float animR = Random.Range(0, 1f);
            myAnimator.Play("Base Layer.bird_loop", 0, animR);

        }

    }
}
