﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class warlusAI : MonoBehaviour
{
    public Animator myAnimator;
    public int waiter;

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = this.gameObject.GetComponent<Animator>();
        getNumbers();
    }

    void getNumbers()
    {
        myAnimator.ResetTrigger("goTale");
        myAnimator.ResetTrigger("goHead");

        int status = Random.Range(1, 99);

        int secAdd = Random.Range(10, waiter);

        StartCoroutine(ExampleCoroutine(secAdd, status));
       //Debug.Log("getNumbers " + secAdd + "|" + status);
    }

    IEnumerator ExampleCoroutine(int seconds, int state)
    {
       // Debug.Log("return " + seconds + "|" + state);

        if (state < 33)
        {
            myAnimator.SetTrigger("goTale");
         //   Debug.Log("<33");
        }

        if (state > 66)
        {
            myAnimator.SetTrigger("goHead");
           // Debug.Log(">66");
        }
        yield return new WaitForSeconds(seconds);
        getNumbers();
    }
}
