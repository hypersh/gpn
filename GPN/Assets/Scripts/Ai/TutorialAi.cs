﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TutorialAi : MonoBehaviour
{
    public float debugFloat;
    public float debugFloat2;
    bool desktop = false;
    public BrowserDetails browser;

    float supertime;

    public RectTransform moveArrows;
    float moveArrowsPos;

    public GameObject skobka_ml;
    public GameObject skobka_mr;
    public GameObject skobka_bl;
    public GameObject skobka_br;
    float timer;

    public RectTransform moveArrows2;
    float moveArrowsPos2;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        supertime = Time.time * 8;

        //desktop move arrows
        moveArrowsPos = Mathf.PingPong(supertime*2, 30);
        moveArrows.transform.localPosition = new Vector2(moveArrowsPos - 180f, moveArrows.transform.localPosition.y);

        //clickSignal
        timer += 1;

        if (timer >= 100f)
        {
            timer = 0f;
        }

        if (timer < 33)
        {
            skobka_ml.SetActive(false);
            skobka_mr.SetActive(false);
            skobka_bl.SetActive(false);
            skobka_br.SetActive(false);
        }
        if (timer > 33 && timer < 66)
        {
            skobka_ml.SetActive(true);
            skobka_mr.SetActive(true);
            skobka_bl.SetActive(false);
            skobka_br.SetActive(false);
        }
        if (timer > 66)
        {
            skobka_ml.SetActive(false);
            skobka_mr.SetActive(false);
            skobka_bl.SetActive(true);
            skobka_br.SetActive(true);
        }

        //desktop move arrows2
        moveArrowsPos2 = Mathf.PingPong(supertime, 8);
        moveArrows2.localPosition = new Vector2(moveArrows2.localPosition.x, moveArrowsPos - 21f);
    }
}
