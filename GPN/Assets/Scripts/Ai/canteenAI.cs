﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canteenAI : MonoBehaviour
{
    public GameObject roof;
    public Transform roof_target;
    public Transform roof_start;
    public Transform roof_end;

    bool canMove = false;
    bool isUP = false;
    bool isDown = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (canMove == true)
        {
            roof.transform.position = Vector3.Lerp(roof.transform.position, roof_target.position, 0.15f);

            if (roof.transform.position.y >= (roof_target.position.y - 0.1f) && isUP == true)
            {
                canMove = false;
            }
            if (roof.transform.position.y <= (roof_target.position.y - 0.1f) && isDown == true)
            {
                canMove = false;
            }

        }
    }

    public void roofGoUp()
    {
        //Debug.Log("roofUp");
        isUP = true;
        isDown = false;
        roof_target = roof_end;
        canMove = true;

    }
    public void roofGoDown()
    {
        //Debug.Log("down");
        isUP = false;
        isDown = true;
        roof_target = roof_start;
        canMove = true;
    }

}
