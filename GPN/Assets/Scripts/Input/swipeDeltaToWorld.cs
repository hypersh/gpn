using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace Lean.Touch
{
    public class swipeDeltaToWorld : MonoBehaviour
    {
        public stateMachine sm;
        bool desktop;

        void Start()
        {
            desktop = sm.desktop;
        }

        public void GetDirection(Vector2 value)
        {
            //Debug.Log(value);
            if (desktop == false)
            {
                if (value.y > 0.2f)
                {
                    sm.StateDOWN();
                }

                if (value.y < -0.2f)
                {
                    sm.stateUp();
                }
            }
        }
    }
}