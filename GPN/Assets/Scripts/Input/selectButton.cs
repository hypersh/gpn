﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class selectButton : MonoBehaviour
{
    public bool iHaveHiperLink = false;
    public GameObject myHiperlink_desktop;
    public GameObject myHiperlink_mobile;
    public GameObject myPanel_desktop;
    public GameObject myPanel_mobile;

    public GameObject globalHeader_desktop;
    public GameObject globalText_desktop;
    public GameObject globalHeader_mobile;
    public GameObject globalText_mobile;

    public BrowserDetails browser;
    public bool iAmDrone = false;
    public GameObject drone;
    public bool iAmAeropark = false;
    public canteenAI cantAi;

    public string myName;
    public stateMachine SM;
    public Button myButton;
    public RectTransform myrect;
    bool desktop = true;
    public Transform myTarget_pVot_D;
    public Transform myTarget_D;

    public Transform myTarget_pVot_M;
    public Transform myTarget_M;

    public string myAnno;
    public string myText;

    float mysize = 1;
    float size;
    // Start is called before the first frame update
    void Start()
    {

        SM = GameObject.FindObjectOfType<stateMachine>();
        desktop = SM.desktop;
        myButton = this.gameObject.GetComponent<Button>();
        myrect = this.gameObject.GetComponent<RectTransform>();

        SM.selectButtons.Add(this);
        if (myTarget_D == null)
        {

            Debug.Log(this.gameObject.name + " HAVE NO TARGET!!!");
        }
        if (browser.trueDesktop == false)
        {
            size = 80f;
            myrect.sizeDelta = new Vector2(size, size);
        }
        if (browser.trueDesktop == true)
        {
            size = 40f;
        }
        if (iAmDrone == true && desktop == true)
        {
            drone.SetActive(false);
        }

        if (iHaveHiperLink == true)
        {
            if (desktop == true)
            {
                myHiperlink_desktop.SetActive(false);
                globalHeader_desktop.SetActive(true);
                globalText_desktop.SetActive(true);
                myPanel_desktop.SetActive(false);
            }
            if (desktop == false)
            {
                myHiperlink_mobile.SetActive(false);
                globalHeader_mobile.SetActive(true);
                globalText_mobile.SetActive(true);
                myPanel_mobile.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        mysize = Mathf.PingPong((Time.time / 4), 0.3f);
        myrect.sizeDelta = new Vector2((mysize + 1) * size, (mysize + 1) * size);
    }

    public void onClick()
    {
        SM.browser.CursorNormal();
        if (desktop == true)
        {
            SM.CM.selectTargetPos = myTarget_D;
            SM.CM.zoomRot = myTarget_D;
            SM.CM.zoomRotator = myTarget_pVot_D;
        }
        if (desktop == false)
        {
            SM.CM.selectTargetPos = myTarget_M;
            SM.CM.zoomRot = myTarget_M;
            SM.CM.zoomRotator = myTarget_pVot_M;
        }
        if (iAmAeropark == true)
        {
            cantAi.roofGoUp();
        }
        if (iAmDrone == true)
        {
            drone.SetActive(true);
        }
        if (iHaveHiperLink == true)
        {
            if (desktop == true)
            {
                myHiperlink_desktop.SetActive(true);
                globalHeader_desktop.SetActive(false);
                globalText_desktop.SetActive(false);
                myPanel_desktop.SetActive(true);
            }
            if (desktop == false)
            {
                myHiperlink_mobile.SetActive(true);
                globalHeader_mobile.SetActive(false);
                globalText_mobile.SetActive(false);
                myPanel_mobile.SetActive(true);
            }
        }
        SM.CM.zoomToTarget();
        SM.textAnno = myAnno;
        SM.textmini = myText;
        SM.ShowAnno();
    }

    public void onHover()
    {
        SM.browser.CursorHover();
    }

    public void UNHover()
    {
        SM.browser.CursorNormal();
    }

    public void resetToMain()
    {
        if (iAmDrone == true && desktop == true)
        {
            drone.SetActive(false);
        }

        myTarget_pVot_D.transform.rotation = new Quaternion(0, 0, 0, 0);
        myTarget_pVot_M.transform.rotation = new Quaternion(0, 0, 0, 0);
        if (iAmAeropark == true)
        {
            cantAi.roofGoDown();
        }
        if (iHaveHiperLink == true)
        {
            if (desktop == true)
            {
                myHiperlink_desktop.SetActive(false);
                globalHeader_desktop.SetActive(true);
                globalText_desktop.SetActive(true);
                myPanel_desktop.SetActive(false);
            }
            if (desktop == false)
            {
                myHiperlink_mobile.SetActive(false);
                globalHeader_mobile.SetActive(true);
                globalText_mobile.SetActive(true);
                myPanel_mobile.SetActive(false);
            }
        }
    }

}
