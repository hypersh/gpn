﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lerpingCamera : MonoBehaviour
{
    // Start is called before the first frame update
    public cameraMchanic CM;
    public bool moving = true;

    public float superfloat;
    public Transform targetGet;
    public Quaternion targetRot;
    public Quaternion myRot;

    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        if (moving == false)
        {
            targetRot = new Quaternion(myRot.x, targetGet.position.z, myRot.z, myRot.w);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, superfloat);
        }
    }
}
