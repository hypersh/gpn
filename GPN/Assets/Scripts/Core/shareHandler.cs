﻿using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

// requaries CallSharerJSPlugin in 
//                                  Assets/Plugins/CallSharerJSPlugin.jslib
public class shareHandler : MonoBehaviour
{
    public bool showDebug = false;
    public string myString;

    //Twitter
    public void share_twitter()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_twitter(" + myString + "))");
        }
        callShareTwitter(myString);
    }
    //Facebook
    public void share_facebook()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_facebook(" + myString + "))");
        }
        callShareFacebook(myString);
    }
    //Linkedin
    public void share_linkedin()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_linkedin(" + myString + "))");
        }
        callShareLinkedin(myString);
    }
    //Email
    public void share_email()
    {
        if (showDebug == true)  
        {
            Debug.Log("Unity send (call_share_email(" + myString + "))");
        }
        callShareEmail(myString);
    }
    //Whatsapp
    public void share_whatsapp()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_whatsapp(" + myString + "))");
        }
        callShareWhatsapp(myString);
    }
    //Telegram
    public void share_telegram()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_telegram(" + myString + "))");
        }
        callShareTelegram(myString);
    }

    //Viber
    public void share_viber()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_viber(" + myString + "))");
        }
        callShareViber(myString);
    }
    //Pinterest
    public void share_pinterest()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_pinterest(" + myString + "))");
        }
        callSharePinterest(myString);
    }
    //Tumblr
    public void share_Tumblr()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_tumblr(" + myString + "))");
        }
        callShareTumblr(myString);
    }
    //Hackernews
    public void share_Hackernews()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_hackernews(" + myString + "))");
        }
        callShareHackernews(myString);
    }
    //Reddit
    public void share_Reddit()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_reddit(" + myString + "))");
        }
        callShareReddit(myString);
    }
    //VK.com
    public void share_VK()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_vk(" + myString + "))");
        }
        callShareVK(myString);
    }
    //Buffer
    public void share_Buffer()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_buffer(" + myString + "))");
        }
        callShareBuffer(myString);
    }
    //Xing
    public void share_Xing()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_xing(" + myString + "))");
        }
        callShareXing(myString);
    }
    //Line
    public void share_Line()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_line(" + myString + "))");
        }
        callShareLine(myString);
    }
    //Instapaper
    public void share_Instapaper()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_instapaper(" + myString + "))");
        }
        callShareInstapaper(myString);
    }
    //Pocket
    public void share_Pocket()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_pocket(" + myString + "))");
        }
        callSharePocket(myString);
    }
    //Digg
    public void share_Digg()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_digg(" + myString + "))");
        }
        callShareDigg(myString);
    }
    //StumbleUpon
    public void share_StumbleUpon()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_stumbleUpon(" + myString + "))");
        }
        callShareStumbleUpon(myString);
    }
    //Flipboard
    public void share_Flipboard()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_flipboard(" + myString + "))");
        }
        callShareFlipboard(myString);
    }
    //Weibo
    public void share_Weibo()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_weibo(" + myString + "))");
        }
        callShareWeibo(myString);
    }
    //Renren
    public void share_Renren()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_renren(" + myString + "))");
        }
        callShareRenren(myString);
    }
    //Myspace
    public void share_Myspace()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_myspace(" + myString + "))");
        }
        callShareMyspace(myString);
    }
    //Blogger
    public void share_Blogger()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_blogger(" + myString + "))");
        }
        callShareBlogger(myString);
    }
    //Baidu
    public void share_Baidu()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_baidu(" + myString + "))");
        }
        callShareBaidu(myString);
    }
    //OK
    public void share_OK()
    {
        if (showDebug == true)
        {
            Debug.Log("Unity send (call_share_OK(" + myString + "))");
        }
        callShareOKRU(myString);
    }

    //EXTERNAL CALLS with CallSharerJSPlugin

    [DllImport("__Internal")]
    private static extern void callShareTwitter(string myString);

    [DllImport("__Internal")]
    private static extern void callShareFacebook(string myString);

    [DllImport("__Internal")]
    private static extern void callShareLinkedin(string myString);

    [DllImport("__Internal")]
    private static extern void callShareEmail(string myString);

    [DllImport("__Internal")]
    private static extern void callShareWhatsapp(string myString);

    [DllImport("__Internal")]
    private static extern void callShareTelegram(string myString);

    [DllImport("__Internal")]
    private static extern void callShareViber(string myString);

    [DllImport("__Internal")]
    private static extern void callSharePinterest(string myString);

    [DllImport("__Internal")]
    private static extern void callShareTumblr(string myString);

    [DllImport("__Internal")]
    private static extern void callShareHackernews(string myString);

    [DllImport("__Internal")]
    private static extern void callShareReddit(string myString);

    [DllImport("__Internal")]
    private static extern void callShareVK(string myString);

    [DllImport("__Internal")]
    private static extern void callShareBuffer(string myString);

    [DllImport("__Internal")]
    private static extern void callShareXing(string myString);

    [DllImport("__Internal")]
    private static extern void callShareLine(string myString);

    [DllImport("__Internal")]
    private static extern void callShareInstapaper(string myString);

    [DllImport("__Internal")]
    private static extern void callSharePocket(string myString);

    [DllImport("__Internal")]
    private static extern void callShareDigg(string myString);

    [DllImport("__Internal")]
    private static extern void callShareStumbleUpon(string myString);

    [DllImport("__Internal")]
    private static extern void callShareFlipboard(string myString);

    [DllImport("__Internal")]
    private static extern void callShareWeibo(string myString);

    [DllImport("__Internal")]
    private static extern void callShareRenren(string myString);

    [DllImport("__Internal")]
    private static extern void callShareMyspace(string myString);

    [DllImport("__Internal")]
    private static extern void callShareBlogger(string myString);

    [DllImport("__Internal")]
    private static extern void callShareBaidu(string myString);
    [DllImport("__Internal")]
    private static extern void callShareOKRU(string myString);
}
