﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMchanic : MonoBehaviour
{
    stateMachine SM;

    public bool desktop = false;
    public float CAM_Moving_speed;
    public float CAM_Moving_speed_vertical;
    public float Cam_freedom;
    public Transform MovingCube;

    float superfloat = 0.1f; //speedmult
    //LevelPositions
    public Transform level_1;
    Vector3 level_1_init_position;
    public Transform level_2;
    Vector3 level_2_init_position;
    public Transform level_3;
    Vector3 level_3_init_position;

    public Transform targetPos;
    public Transform selectTargetPos; //selec tTarget

    //cameraRotationSystem
    public bool canRotate = false;

    public Quaternion Camera_startRot;
    public Transform zoomRot;
    public Transform zoomRotator;
    public Transform mainRotator;
    public Quaternion targetRot;


    public Transform mainCamera;

    [Min(1f)]
    public float cameraZoomNormal;
    public float cameraZoomZoomed_horizontal;
    public float cameraZoomZoomed_vertical;
    float zoomTarget = 1;
    // Start is called before the first frame update
    void Start()
    {
        SM = this.gameObject.GetComponent<stateMachine>();

        Camera_startRot = mainCamera.rotation;
        level_1_init_position = level_1.position;
        level_2_init_position = level_2.position;
        level_3_init_position = level_3.position;
        targetRot = Camera_startRot;
    }

    // Update is called once per frame
    void Update()
    {
        //main camera Lerp
        mainCamera.position = Vector3.Lerp(mainCamera.position, targetPos.position, CAM_Moving_speed);
        mainCamera.rotation = Quaternion.Lerp(mainCamera.rotation, targetRot, superfloat);

        SM.browser.zoomer = Mathf.Lerp(SM.browser.zoomer, zoomTarget, CAM_Moving_speed / 1.5f);

        if (canRotate == true)
        {
            // Camera_targetRot = new Quaternion(rotTargetPos.transform.rotation.x, MovingCube.position.z, rotTargetPos.transform.rotation.z, 180f);
            targetRot = zoomRot.rotation;
            zoomRotator.transform.rotation = new Quaternion(zoomRotator.transform.rotation.x, -(MovingCube.position.z * 1.5f), zoomRotator.transform.rotation.z, 180f);
            //mainRotator.transform.rotation = new Quaternion(mainRotator.transform.rotation.x, MovingCube.position.z * 1.5f, mainRotator.transform.rotation.z, 180f);
        }
        if (canRotate == false)
        {
            level_1.localPosition = new Vector3((MovingCube.localPosition.z * Cam_freedom) + level_1_init_position.x, level_1.position.y, (-MovingCube.localPosition.z * Cam_freedom) + level_1_init_position.z);
            level_2.localPosition = new Vector3((MovingCube.localPosition.z * Cam_freedom) + level_2_init_position.x, level_2.position.y, (-MovingCube.localPosition.z * Cam_freedom) + level_2_init_position.z);
            level_3.localPosition = new Vector3((MovingCube.localPosition.z * Cam_freedom) + level_3_init_position.x, level_3.position.y, (-MovingCube.localPosition.z * Cam_freedom) + level_3_init_position.z);
        }

    }

    public void zoomToTarget()
    {
        MovingCube.position = new Vector3(0, 0, 0);
        if (SM.state == 2)
        {
            SM.water.SetActive(false);
        }
        if (SM.browser.orientisHorizontal == true)
        {
            zoomTarget = cameraZoomZoomed_horizontal;
        }
        if (SM.browser.orientisHorizontal == false)
        {
            zoomTarget = cameraZoomZoomed_vertical;
        }
        canRotate = true;
        targetPos = selectTargetPos;
    }

    public void zoomBack()
    {
        targetRot = Camera_startRot;
        canRotate = false;
        zoomTarget = cameraZoomNormal;
        MovingCube.position = new Vector3(0, 0, 0);
        if (SM.state == 1)
        {
            targetPos.position = level_1.localPosition;
        }
        if (SM.state == 2)
        {
            targetPos.position = level_2.localPosition;
        }
        if (SM.state == 3)
        {
            targetPos.position = level_3.localPosition;
        }
    }
}
