﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

public class BrowserDetails : MonoBehaviour
{
    //debug text
    public Text debugText;
    public Text debugTextOS;
    //objects
    public GameObject desktop_UI;
    public GameObject mobile_UI;
    public GameObject shared_UI;
    //Logic
    public stateMachine SM;
    public cameraMchanic CM;
    public GameObject drone;
    //display
    [Min(1f)]
    public float zoomer;
    public float aspect;
    public float debugMult0m4;
    public float debugMult0m3;
    public float debugMult0m2;
    public float debugMult0m1;
    public float debugMult0;
    public float debugMult0_1;
    public float debugMult1;
    public float debugMult1_1;
    public float camFreedom_1;
    public float debugMult2;
    public float debugMult2_1;
    public float camFreedom_2;
    public float debugMult3;
    public float camFreedom_3;
    public float debugMult4;
    public float camFreedom_4;
    public float debugMult5;
    public float camFreedom_5;
    public float debugMult6;
    public float camFreedom_6;
    public float debugMult7;
    public float camFreedom_7;
    public float debugMult8;
    public float camFreedom_8;
    public float debugMult9;
    public float camFreedom_9;

    public Camera mainCamera;
    //
    public bool trueDesktop = false;
    public bool mac = false;
    public bool unknownDevice = true;
    public bool debugTouch = false;

    public bool orientisHorizontal;

    string OSstring;
    string deviceType;
    //os STRINGS
    string superStringForTEXT;
    //DESKTOP
    string win10 = "Windows 10";
    string win8 = "Windows 8";
    string win7 = "Windows 7";

    string macOS = "Mac OS";

    string linux = "linux";

    //cursor
    public Texture2D mainCursor;
    public Texture2D hoverCursor;

    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    void Awake()
    {
        trueDesktop = false;
        unknownDevice = true;
        drone.SetActive(false);
        if (debugTouch == false)
        {
            OSstring = SystemInfo.operatingSystem;

            if (SystemInfo.deviceType == DeviceType.Console)
            {
                //Change the text of the label
                deviceType = "Console";
            }

            //Check if the device running this is a desktop
            if (SystemInfo.deviceType == DeviceType.Desktop)
            {
                deviceType = "Desktop";
            }

            //Check if the device running this is a handheld
            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                deviceType = "Handheld";
            }

            //Check if the device running this is unknown
            if (SystemInfo.deviceType == DeviceType.Unknown)
            {
                deviceType = "Unknown";
            }

            superStringForTEXT = deviceType + " | " + OSstring + " | " + SystemInfo.processorType + " | " + SystemInfo.graphicsDeviceName + " | shaderLVL | " + SystemInfo.graphicsShaderLevel;
            //windows check
            if (OSstring.IndexOf(win10) > -1)
            {
                trueDesktop = true;
                unknownDevice = false;
            }
            if (OSstring.IndexOf(win8) > -1)
            {
                trueDesktop = true;
                unknownDevice = false;
            }
            if (OSstring.IndexOf(win7) > -1)
            {
                trueDesktop = true;
                unknownDevice = false;
            }

            //mac Check
            if (OSstring.IndexOf(macOS) > -1)
            {
                trueDesktop = true;
                unknownDevice = false;
                mac = true;
                SM.mac = true;
                SM.mainLight.shadows = LightShadows.None;
            }
            //linux Check
            if (OSstring.IndexOf(linux) > -1)
            {
                trueDesktop = true;
                unknownDevice = false;

            }
            ///////////////////////////////// END OF OS CHECK
            ///
            //GPU SWITCH --- IPAD
            debugTextOS.text = "gpu parsed";
            if (SystemInfo.graphicsDeviceName.IndexOf("Apple") > -1)
            {
                trueDesktop = false;
                unknownDevice = true;
                debugTextOS.text = "we get Apple GPU";
            }

        }

        if (debugTouch == true) ///////////////////////// DEBUG SWITCH CODE
        {
            trueDesktop = false;
            unknownDevice = true;
        }
        runGame();
    }
    void runGame()
    {
        shared_UI.SetActive(false);
        if (trueDesktop == true && debugTouch == false)
        {
            Cursor.SetCursor(mainCursor, hotSpot, cursorMode);
            SM.desktop = true;
            CM.desktop = true;
            desktop_UI.SetActive(true);
            mobile_UI.SetActive(false);
            drone.SetActive(false);
        }
        if (trueDesktop == false || debugTouch == true)
        {
            SM.desktop = false;
            CM.desktop = false;
            desktop_UI.SetActive(false);
            mobile_UI.SetActive(true);
            drone.SetActive(true);
        }
        //superStringForTEXT = deviceType + " | " + OSstring + " | " + SystemInfo.processorType + " | " + SystemInfo.graphicsDeviceName + " | shaderLVL | " + SystemInfo.graphicsShaderLevel;
        debugText.text = superStringForTEXT;
        ///debugTextOS.text = SystemInfo.deviceType.ToString();
    }




    // Update is called once per frame
    void Update()
    {
        if (Screen.height > Screen.width)
        {
            orientisHorizontal = false;
            //aspect = (Screen.height * 1f) / (Screen.width * 1f);
            if (trueDesktop == false)
            {

            }
        }
        if (Screen.width > Screen.height)
        {
            //aspect = (Screen.width * 1f) / (Screen.height * 1f);
            orientisHorizontal = true;

            if (trueDesktop == false)
            {

            }
        }
        //aspect calc

        aspect = (Screen.height * 1f) / (Screen.width * 1f);

        if (aspect < 0.3f)
        {
            mainCamera.orthographicSize = aspect * (debugMult0m4 / zoomer);
            CM.Cam_freedom = camFreedom_1;
        }
        if (aspect > 0.3f && aspect < 0.35f)
        {
            mainCamera.orthographicSize = aspect * (debugMult0m3 / zoomer);
            CM.Cam_freedom = camFreedom_1;
        }
        if (aspect > 0.35f && aspect < 0.4f)
        {
            mainCamera.orthographicSize = aspect * (debugMult0m2 / zoomer);
            CM.Cam_freedom = camFreedom_1;
        }
        if (aspect > 0.4f && aspect < 0.5f)
        {
            mainCamera.orthographicSize = aspect * (debugMult0m1 / zoomer);
            CM.Cam_freedom = camFreedom_1;
        }
        if (aspect > 0.5f && aspect < 0.6f)
        {
            mainCamera.orthographicSize = aspect * (debugMult0_1 / zoomer);
            CM.Cam_freedom = camFreedom_2;
        }
        if (aspect > 0.6f && aspect < 0.7f)
        {
            mainCamera.orthographicSize = aspect * (debugMult1 / zoomer);
            CM.Cam_freedom = camFreedom_2;
        }
        if (aspect > 0.7f && aspect < 0.8f)
        {
            mainCamera.orthographicSize = aspect * (debugMult1_1 / zoomer);
            CM.Cam_freedom = camFreedom_2;
        }
        if (aspect > 0.8f && aspect < 0.9f)
        {
            mainCamera.orthographicSize = aspect * (debugMult2 / zoomer);
            CM.Cam_freedom = camFreedom_3;
        }
        if (aspect > 0.9f && aspect < 1f)
        {
            mainCamera.orthographicSize = aspect * (debugMult2_1 / zoomer);
            CM.Cam_freedom = camFreedom_3;
        }
        if (aspect > 1f && aspect < 1.2f)
        {
            mainCamera.orthographicSize = aspect * (debugMult3 / zoomer);
            CM.Cam_freedom = camFreedom_4;
        }
        if (aspect > 1.2f && aspect < 1.4f)
        {
            mainCamera.orthographicSize = aspect * (debugMult4 / zoomer);
            CM.Cam_freedom = camFreedom_5;
        }
        if (aspect > 1.4f && aspect < 1.6f)
        {
            mainCamera.orthographicSize = aspect * (debugMult5 / zoomer);
            CM.Cam_freedom = camFreedom_6;
        }
        if (aspect > 1.6f && aspect < 1.8f)
        {
            mainCamera.orthographicSize = aspect * (debugMult6 / zoomer);
            CM.Cam_freedom = camFreedom_7;
        }
        if (aspect > 1.8f && aspect < 2f)
        {
            mainCamera.orthographicSize = aspect * (debugMult7 / zoomer);
            CM.Cam_freedom = camFreedom_8;
        }
        if (aspect > 2f && aspect < 2.2f)
        {
            mainCamera.orthographicSize = aspect * (debugMult8 / zoomer);
            CM.Cam_freedom = camFreedom_9;
        }
        if (aspect > 2.2f)
        {
            mainCamera.orthographicSize = aspect * (debugMult9 / zoomer);
            CM.Cam_freedom = camFreedom_9;
        }

    }

    public void CursorNormal()
    {
        if (trueDesktop == true && debugTouch == false)
        {
            Cursor.SetCursor(mainCursor, hotSpot, cursorMode);
        }
    }
    public void CursorHover()
    {
        if (trueDesktop == true && debugTouch == false)
        {
            Cursor.SetCursor(hoverCursor, hotSpot, cursorMode);
        }
    }

}
