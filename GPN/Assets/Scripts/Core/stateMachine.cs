﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;


public class stateMachine : MonoBehaviour
{
    public cameraMchanic CM;
    public BrowserDetails browser;

    public Light mainLight;

    public bool desktop = false;
    public bool mac = false;
    public float speed;

    public int state;
    public bool zoomLock = true;
    bool locker = false;

    public List<selectButton> selectButtons = new List<selectButton>();

    public GameObject buttons_1;
    public GameObject buttons_2;
    public GameObject buttons_3;
    public GameObject water;

    public panelsMechanic panelsD;
    public panelsMechanic panelsM;

    public string textAnno;
    public string textmini;

    public Text textObject_anno_d;
    public Text textObject_mini_d;
    public Text textObject_anno_m;
    public Text textObject_mini_m;

    public Text textObject_anno;
    public Text textObject_mini;

    bool wheelLocked = false;
    float mouseCounter;
    float chk = 1f;

    public GameObject arrows_d;
    public GameObject arrows_m;
    public GameObject arrows;

    public GameObject arrowUP_d;
    public GameObject arrowUP_m;
    GameObject arrowUP;

    public GameObject arrowDOWN_d;
    public GameObject arrowDOWN_m;
    GameObject arrowDOWN;

    // Start is called before the first frame update
    void Start()
    {
        CM = this.gameObject.GetComponent<cameraMchanic>();
        browser = CM.GetComponent<BrowserDetails>();
        state = 1;

        if (desktop == true)
        {
            textObject_anno = textObject_anno_d;
            textObject_mini = textObject_mini_d;

            arrows = arrows_d;
            arrowUP = arrowUP_d;
            arrowDOWN = arrowDOWN_d;
        }
        if (desktop == false)
        {
            textObject_anno = textObject_anno_m;
            textObject_mini = textObject_mini_m;

            arrows = arrows_m;
            arrowUP = arrowUP_m;
            arrowDOWN = arrowDOWN_m;

        }
        state_1();
        arrows.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (mouseCounter > 0)
        {
            mouseCounter -= 0.1f;
        }

        if (mouseCounter <= 0)
        {
            mouseCounter = 0;
        }


    }

    public void StateDOWN()
    {
        if (zoomLock == false)
        {
            if (state == 1 && locker == false)
            {
                locker = true;
                state = 2;
                state_2();
            }

            if (state == 2 && locker == false)
            {
                locker = true;
                state = 3;
                state_3();
            }
            locker = false;
        }
    }
    public void stateUp()
    {
        if (zoomLock == false)
        {
            if (state == 3 && locker == false)
            {
                locker = true;
                state = 2;
                state_2();
            }

            if (state == 2 && locker == false)
            {
                locker = true;
                state = 1;
                state_1();
            }
            locker = false;
        }
    }

    void state_1()
    {
        arrowUP.SetActive(false);
        arrowDOWN.SetActive(false);

        CM.targetPos = CM.level_1;
        buttons_1.SetActive(true);
        buttons_2.SetActive(false);
        buttons_3.SetActive(false);
        if (desktop == true && mac == false)
        {
            mainLight.shadows = LightShadows.Soft;
        }
        if (desktop == false)
        {
            mainLight.shadows = LightShadows.None;
        }

        arrowUP.SetActive(false);
        arrowDOWN.SetActive(true);

    }
    void state_2()
    {
        arrowUP.SetActive(false);
        arrowDOWN.SetActive(false);

        CM.targetPos = CM.level_2;
        buttons_1.SetActive(false);
        buttons_2.SetActive(true);
        buttons_3.SetActive(false);
        mainLight.shadows = LightShadows.None;

        arrowUP.SetActive(true);
        arrowDOWN.SetActive(true);
    }
    void state_3()
    {
        arrowUP.SetActive(false);
        arrowDOWN.SetActive(false);

        CM.targetPos = CM.level_3;
        buttons_1.SetActive(false);
        buttons_2.SetActive(false);
        buttons_3.SetActive(true);
        mainLight.shadows = LightShadows.None;

        arrowUP.SetActive(true);
        arrowDOWN.SetActive(false);
    }

    public void ShowAnno()
    {
        zoomLock = true;
        buttons_1.SetActive(false);
        buttons_2.SetActive(false);
        buttons_3.SetActive(false);

        if (desktop == true)
        {
            panelsD.showPanel();
        }

        if (desktop == false)
        {
            panelsM.showPanel();
        }
        FillAnno();
        wheelLocked = true;
        arrows.SetActive(false);
    }

    public void main()
    {
        if (desktop == true)
        {
            panelsD.hidePanel();
        }

        if (desktop == false)
        {
            panelsM.hidePanel();
        }
        arrows.SetActive(true);
        water.SetActive(true);
        zoomLock = false;

        if (state == 1)
        {
            state_1();
        }
        if (state == 2)
        {
            state_2();
        }
        if (state == 3)
        {
            state_3();
        }
        CM.zoomBack();
        browser.CursorNormal();
        foreach (selectButton b in selectButtons)
        {
            b.resetToMain();
        }
        wheelLocked = false;
    }

    void FillAnno()
    {
        textObject_anno.text = textAnno;
        textObject_mini.text = textmini;
    }

    public void OnGUI()
    {
        if (Event.current.isScrollWheel)
        {
            WheelScaler(-(Event.current.delta.y));
        }
    }

    public void WheelScaler(float delta)
    {
        if (wheelLocked == false && desktop == true)
        {
            if (mouseCounter < 0.5f)
            {
                if (delta > 0.01f)
                {
                    stateUp();
                }
                if (delta < 0.01f)
                {
                    StateDOWN();
                }
            }
            mouseCounter = 1.5f;
        }
    }

}
