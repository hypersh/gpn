﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class socialButtons : MonoBehaviour
{
    public bool share_VK = false;
    public bool share_TG = false;
    public bool share_FB = false;
    public bool share_OK = false;
    public bool share_WA = false;
    public bool share_TW = false;

    public bool iamLife = false;

    public bool iamSocials = false;

    public socialPanels social;
    public stateMachine SM;
    public BrowserDetails browser;
    public RectTransform myRect;
    public Image muImage;
    shareHandler sh;
    shareJsConnector sJs;
    Vector2 mySize;
    Vector2 BigSize;
    bool desktop;
    // Start is called before the first frame update
    void Start()
    {
        desktop = browser.trueDesktop;
        sh = browser.GetComponent<shareHandler>();
        mySize = myRect.sizeDelta;
        BigSize = new Vector2(mySize.x * 1.5f, mySize.y * 1.5f);

    }

    public void Click()
    {
        if (share_VK == true)
        {
          
            sJs.Open("http://vk.com/share.php?url=https%3A%2F%2Fgidropolis.life.ru…%2F%2Fgidropolis.life.ru%2FTemplateData%2Fimages%2Fshare.jpg");
        }
        if (share_TG == true)
        {
            
            sJs.Open("tg://msg_url?text=%D0%9E%D0%BA%D0%B5%D0%B0%D0%BD%20%E2%80%94%20%D0%B1%D0%B5%D1%81%D0%BA%D0%BE%D0%BD%D0%B5%D1%87%D0%BD%D1%8B%D0%B9%20%D0%B8%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B8%D0%BA%20%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8.%20%D0%9C%D1%8B%20%D0%B7%D0%B0%D0%B3%D0%BB%D1%8F%D0%BD%D1%83%D0%BB%D0%B8%20%D0%B2%20%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B5%20%D0%B8%20%D0%BF%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%B8%D0%BB%D0%B8%2C%20%D0%BA%D0%B0%D0%BA%20%D0%BC%D0%BE%D0%B3%20%D0%B1%D1%8B%20%D0%B2%D1%8B%D0%B3%D0%BB%D1%8F%D0%B4%D0%B5%D1%82%D1%8C%20%D0%B8%20%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D0%BE%D0%BD%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%20%D0%BF%D0%BE%D0%B4%D0%B2%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9%20%D0%B3%D0%BE%D1%80%D0%BE%D0%B4.%20%D0%9D%D0%B0%D1%88%D0%B8%20%D0%B7%D0%B0%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BA%D0%B8%20%E2%80%94%20%D0%BD%D0%B5%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%20%D1%84%D0%B0%D0%BD%D1%82%D0%B0%D0%B7%D0%B8%D1%8F%2C%20%D0%B2%20%D0%B8%D1%85%20%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%B5%20%D1%80%D0%B5%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5%20%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8%2C%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5%20%D1%83%D0%B6%D0%B5%20%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F%20%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D1%8F%D1%8E%D1%82%D1%81%D1%8F.&url=https%3A%2F%2Fgidropolis.life.ru&");
        }
        if (share_FB == true)
        {
            
            sJs.Open("https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fgidropolis.life.ru&&");
        }
        if (share_OK == true)
        {
            
            sJs.Open("https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareU…%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D1%8F%D1%8E%D1%82%D1%81%D1%8F.");
        }
        if (share_WA == true)
        {
            
            sJs.Open("whatsapp://send?text=%D0%9E%D0%BA%D0%B5%D0%B0%D0%BD%20%E2%80%94%20%D0%B1%D0%B5%D1%81%D0%BA%D0%BE%D0%BD%D0%B5%D1%87%D0%BD%D1%8B%D0%B9%20%D0%B8%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B8%D0%BA%20%D0%B6%D0%B8%D0%B7%D0%BD%D0%B8.%20%D0%9C%D1%8B%20%D0%B7%D0%B0%D0%B3%D0%BB%D1%8F%D0%BD%D1%83%D0%BB%D0%B8%20%D0%B2%20%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B5%20%D0%B8%20%D0%BF%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%B8%D0%BB%D0%B8%2C%20%D0%BA%D0%B0%D0%BA%20%D0%BC%D0%BE%D0%B3%20%D0%B1%D1%8B%20%D0%B2%D1%8B%D0%B3%D0%BB%D1%8F%D0%B4%D0%B5%D1%82%D1%8C%20%D0%B8%20%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D0%BE%D0%BD%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C%20%D0%BF%D0%BE%D0%B4%D0%B2%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9%20%D0%B3%D0%BE%D1%80%D0%BE%D0%B4.%20%D0%9D%D0%B0%D1%88%D0%B8%20%D0%B7%D0%B0%D1%80%D0%B8%D1%81%D0%BE%D0%B2%D0%BA%D0%B8%20%E2%80%94%20%D0%BD%D0%B5%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%20%D1%84%D0%B0%D0%BD%D1%82%D0%B0%D0%B7%D0%B8%D1%8F%2C%20%D0%B2%20%D0%B8%D1%85%20%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%B5%20%D1%80%D0%B5%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5%20%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8%2C%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5%20%D1%83%D0%B6%D0%B5%20%D1%81%D0%B5%D0%B3%D0%BE%D0%B4%D0%BD%D1%8F%20%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D1%8F%D1%8E%D1%82%D1%81%D1%8F.%20https%3A%2F%2Fgidropolis.life.ru");
        }
        if (share_TW == true)
        {
            
            sJs.Open("https://twitter.com/intent/tweet/?text=%D0%9E%D0%BA%D0%B5%D0…8E%D1%82%D1%81%D1%8F.&url=https%3A%2F%2Fgidropolis.life.ru&&");
        }
        if (iamSocials == true)
        {
            social.Signal();
        }
    }

    public void HOVER()
    {
        if (desktop == true)
        {
            browser.CursorHover();
            if (iamLife == false)
            {
                myRect.sizeDelta = BigSize;
            }
        }
    }
    public void unHOVER()
    {
        if (desktop == true)
        {
            browser.CursorNormal();
            if (iamLife == false)
            {
                myRect.sizeDelta = mySize;
            }
        }
    }
}
