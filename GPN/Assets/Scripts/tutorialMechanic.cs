﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialMechanic : MonoBehaviour
{
    public BrowserDetails browser;
    public stateMachine sm;
    public GameObject d2;
    public GameObject tutorDesktop;
    public GameObject tutorMobile;
    public GameObject water;
    public void closeTutor()
    {
        if(browser.trueDesktop == true)
        {
            Destroy(tutorDesktop);
        }

        if (browser.trueDesktop == false)
        {
            Destroy(tutorMobile);
        }
        water.SetActive(true);
        browser.shared_UI.SetActive(true);
        browser.CursorNormal();
        d2.SetActive(true);
        sm.arrows.SetActive(true);
        sm.zoomLock = false;
    }

}
