﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class socialPanels : MonoBehaviour
{
    public bool hideRights;
    public GameObject rights;
    public GameObject myPanel;
    public Transform panel_open;
    public Transform panel_close;
    bool locker = false;
    bool openBool = false;
    Transform bufferTransform;

    // Start is called before the first frame update
    void Start()
    {
        bufferTransform = panel_close;
    }

    // Update is called once per frame
    void Update()
    {
        myPanel.transform.position = Vector3.Lerp(myPanel.transform.position, bufferTransform.position, 0.1f);
    }


    public void Signal()
    {
        if (openBool == false && locker == false)
        {
            locker = true;
            bufferTransform = panel_open;
            openBool = true; if (hideRights == true)
            {
                rights.SetActive(false);
            }
        }

        if (openBool == true && locker == false)
        {
            locker = true;
            bufferTransform = panel_close;
            openBool = false;
            if (hideRights == true)
            {
                rights.SetActive(true);
            }
        }

        locker = false;
    }
}
