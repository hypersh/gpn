﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonHover : MonoBehaviour
{
    public BrowserDetails browser;
    public stateMachine SM;
    tutorialMechanic TM;

    public bool tutorButton = false;
    public bool tutorButton2 = false;
    public float scalerMult;
    Vector3 initScale;
    Vector3 addScale;
    bool desktop = false;
    // Start is called before the first frame update
    void Start()
    {
        SM = FindObjectOfType<stateMachine>();
        browser = SM.GetComponent<BrowserDetails>();
        TM = SM.GetComponent<tutorialMechanic>();
        initScale = this.gameObject.transform.localScale;
        addScale = new Vector3(initScale.x * scalerMult, initScale.y * scalerMult, initScale.z * scalerMult);

        if (browser.trueDesktop == true)
        {
            desktop = true;
        }
        if (browser.trueDesktop == false)
        {
            desktop = false;
        }

    }

    public void Click()
    {
        if (desktop == true)
        {
            transform.localScale = initScale;
        }
        if (tutorButton == false)
        {
            SM.main();
        }
        if (tutorButton == true)
        {
            TM.closeTutor();
        }

    }
    public void HOVER()
    {
        if (desktop == true)
        {
            if (tutorButton2 == false)
            {
                transform.localScale = addScale;
            }
            browser.CursorHover();
        }
    }
    public void unHover()
    {
        if (desktop == true)
        {
            browser.CursorNormal();
            if (tutorButton2 == false)
            {
                transform.localScale = initScale;
            }
        }
    }
}
