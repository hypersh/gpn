﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class panelsMechanic : MonoBehaviour
{
    public Transform panelClosed;
    public Transform panelOpen;
    public Transform bufferTransform;

    public Transform Panel;


    // Start is called before the first frame update
    void Start()
    {
        Panel.position = panelClosed.localPosition;
        bufferTransform = panelClosed;
    }

    // Update is called once per frame
    void Update()
    {
        Panel.localPosition = Vector3.Lerp(Panel.localPosition, bufferTransform.localPosition, 0.1f);

    }

    public void showPanel()
    {
        bufferTransform = panelOpen;
    }
    public void hidePanel()
    {
        bufferTransform = panelClosed;
    }
}
