﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class openurl : MonoBehaviour
{
    public string myurl;
    public BrowserDetails browser;

    public void onClick()
    {
       // sh.openURL(myurl);

        if (browser.trueDesktop == true)
        {
            openWindow(myurl);
        }
        if (browser.trueDesktop == false)
        {
            Application.OpenURL(myurl);
        }

    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
