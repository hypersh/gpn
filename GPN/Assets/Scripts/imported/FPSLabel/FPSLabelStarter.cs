﻿//#if UNITY_EDITOR || DEVELOPMENT_BUILD
using UnityEngine;

public static class FPSLabelStarter
{
	

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
	private static void OnAfterSceneLoadRuntimeMethod()
	{
		bool startFps = false;
		FPSLabel fpsLabel = GameObject.FindObjectOfType<FPSLabel>();
		if (fpsLabel == null && startFps == true)
		{
			GameObject fpsLabelGO = new GameObject("FPS Label");
			fpsLabelGO.AddComponent<FPSLabel>();
		}
	}
}
//#endif