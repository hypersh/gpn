var CallSharerJSPlugin = {
    
	saveURL: function (urlString) 
	{
		window.saveURL(Pointer_stringify(urlString));
	},

	saveTitle: function (titleString) 
	{
		window.saveTitle(Pointer_stringify(titleString));
	},

	callShareTwitter: function (myString) 
	{
		window.call_share_twitter(Pointer_stringify(myString));
	},

  	callShareFacebook: function (myString) 
	{
		window.call_share_facebook(Pointer_stringify(myString));
	},	
	
	callShareLinkedin: function (myString) 
	{
		window.call_share_linkedin(Pointer_stringify(myString));
	},
	
	callShareEmail: function (myString) 
	{
		window.call_share_email(Pointer_stringify(myString));
	},

	callShareWhatsapp: function (myString) 
	{
		window.call_share_whatsapp(Pointer_stringify(myString));
	},

	callShareTelegram: function (myString) 
	{
		window.call_share_telegram(Pointer_stringify(myString));
	},

	callShareViber: function (myString) 
	{
		window.call_share_viber(Pointer_stringify(myString));
	},

	callSharePinterest: function (myString) 
	{
		window.call_share_pinterest(Pointer_stringify(myString));
	},

	callShareTumblr: function (myString) 
	{
		window.call_share_tumblr(Pointer_stringify(myString));
	},
	
	callShareHackernews: function (myString) 
	{
		window.call_share_hackernews(Pointer_stringify(myString));
	},

	callShareReddit: function (myString) 
	{
		window.call_share_reddit(Pointer_stringify(myString));
	},

	callShareVK: function (myString) 
	{
		window.call_share_vk(Pointer_stringify(myString));
	},

	callShareBuffer: function (myString) 
	{
		window.call_share_buffer(Pointer_stringify(myString));
	},

	callShareXing: function (myString) 
	{
		window.call_share_xing(Pointer_stringify(myString));
	},

	callShareLine: function (myString) 
	{
		window.call_share_line(Pointer_stringify(myString));
	},
	
	callShareInstapaper: function (myString) 
	{
		window.call_share_instapaper(Pointer_stringify(myString));
	},

	callSharePocket: function (myString) 
	{
		window.call_share_pocket(Pointer_stringify(myString));
	},

	callShareDigg: function (myString) 
	{
		window.call_share_digg(Pointer_stringify(myString));
	},
		
	callShareStumbleUpon: function (myString) 
	{
		window.call_share_stumbleUpon(Pointer_stringify(myString));
	},

	callShareFlipboard: function (myString) 
	{
		window.call_share_flipboard(Pointer_stringify(myString));
	},

	callShareWeibo: function (myString) 
	{
		window.call_share_weibo(Pointer_stringify(myString));
	},
	
	callShareRenren: function (myString) 
	{
		window.call_share_renren(Pointer_stringify(myString));
	},
		
	callShareMyspace: function (myString) 
	{
		window.call_share_myspace(Pointer_stringify(myString));
	},

	callShareBlogger: function (myString) 
	{
		window.call_share_blogger(Pointer_stringify(myString));
	},

	callShareBaidu: function (myString) 
	{
		window.call_share_baidu(Pointer_stringify(myString));
	},
		callShareOKRU: function (myString) 
	{
		window.call_share_okru(Pointer_stringify(myString));
	},

};

mergeInto(LibraryManager.library, CallSharerJSPlugin);