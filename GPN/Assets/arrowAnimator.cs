﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowAnimator : MonoBehaviour
{
    public bool desktop;
    public stateMachine SM;
    public BrowserDetails browser;
    public Transform mytransform;
    float myScaler;
    float myZ = 1f;
    float myTime;

    int state;
    void Start()
    {
        mytransform = this.GetComponent<RectTransform>();
    }


    void Update()
    {
        myTime = (Time.time / 8);
        myScaler = Mathf.PingPong(myTime, 0.15f);

        mytransform.localScale = new Vector3(myScaler +1f, myScaler+1f, myZ);
    }

    public void ONCLICK_UP()
    {
        state = SM.state;
        if (state == 2)
        {
            browser.CursorNormal();
        }
    }
    public void ONCLICK_DOWN()
    {
        state = SM.state;
        if (state == 2)
        {
            browser.CursorNormal();
        }
    }
}
