﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class shareJsConnector : MonoBehaviour
{
    public bool getLinks;
    public BrowserDetails browser;
    public shareHandler sh;
    public bool nativeOpen = false;
    bool nativeLock = false;

    public string myUrl;
    public string shareUrl;
    public shareHandler SH;

    // Start is called before the first frame update
    void Start()
    {
        sh = browser.GetComponent<shareHandler>();
       // SendData();
        if (getLinks == true)
        {
            sh.share_VK();
            sh.share_telegram();
            sh.share_facebook();
            sh.share_OK();
            sh.share_whatsapp();
            sh.share_twitter();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SendData()
    {
        //Debug.Log("Sending Data:  ");
        if (SH.myString != "")
        {
          //  Debug.Log("Sending title: " + SH.myString);
            saveTitle(SH.myString);
        }
        saveURL(shareUrl);
       // Debug.Log("Sending url: " + shareUrl);
    }

    public void getUrl(string tempUrl) // function called with java
    {
      //  Debug.Log("saving: " + tempUrl + " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        myUrl = tempUrl;
      //  Debug.Log("saved: " + myUrl + " ++++++++++++++++++++++++");
    }

    //open share window
    public void Open(string urlOpen)
    {
        if (myUrl != "")
        {
            if (browser.trueDesktop == true)
            {
               // Debug.Log("Opening: " + urlOpen);
                openWindow(urlOpen);
            }
            if (browser.trueDesktop == false)
            {
               // Debug.Log("Opening: " + urlOpen);
                Application.OpenURL(urlOpen);
            }
        }
    }
    [DllImport("__Internal")]
    private static extern void saveURL(string shareURL);

    [DllImport("__Internal")]
    private static extern void saveTitle(string shareTitle);

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
