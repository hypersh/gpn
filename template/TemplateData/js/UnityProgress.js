function UnityProgress(gameInstance, progress) {
    if (!gameInstance.Module)
        return;

    const waterFill = document.querySelector('.water-fill');
    var percent = Math.round(progress * 100);
    var new_percent = 90;
    if (progress < 0.9){
        document.querySelector("#progress_text").innerHTML = percent + '%';
    }
    else if (progress == 0.9) {
        var new_percent = [90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
        var id_array = 0;
        load_smooth();
        function load_smooth() {
            document.querySelector("#progress_text").innerHTML = new_percent[id_array] + '%';
            id_array++;
            if (new_percent.length > id_array) {
                setTimeout(load_smooth, 600);
            }
        }
    }
    waterFill.style.height = `${60 * progress}px`;
    waterFill.style.y = `${50 - 60 * progress}px`;
    if (progress == "complete") {
        document.querySelector("#progress").style.display = 'none';
        document.querySelector("#start_btn").style.display = 'inline-block';
    } else if (progress == 1) {
        document.querySelector("#progress").style.display = 'none';
        document.querySelector("#start_btn").style.display = 'inline-block';
        document.querySelector("#gameContainer").style.display = 'none';
    } 
}

function start() {
    document.querySelector("header").style.display = 'none';
    document.querySelector("footer").style.display = 'none';
    document.querySelector(".counter").style.display = 'none';
    document.querySelector("#gameContainer").style.display = 'block';
}

// Получение необходимого URL
window.saveURL = (url) => {
    var test_console = url;
    // console.log("url = ", url);
    desired_url = url;

    $("#vk-button").attr('data-url', desired_url);
    
    // share III tipe
    $("#tumblr-button").attr('data-url', desired_url);
    $("#instapaper-button").attr('data-url', desired_url);
    $("#myspace-button").attr('data-url', desired_url);
    $("#blogger-button").attr('data-url', desired_url);

    // share II tipe
    $("#twitter-button").attr('data-url', desired_url);
    $("#telegram-button").attr('data-url', desired_url);
    $("#whatsapp-button").attr('data-url', desired_url);
    $("#viber-button").attr('data-url', desired_url);
    $("#hackernews-button").attr('data-url', desired_url);
    $("#stumbleupon-button").attr('data-url', desired_url);
    $("#flipboard-button").attr('data-url', desired_url);
    $("#okru-button").attr('data-url', desired_url);

    // share I tipe
    $("#linkedin-button").attr('data-url', desired_url);
    $("#facebook-button").attr('data-url', desired_url);
    $("#reddit-button").attr('data-url', desired_url);
    $("#pocket-button").attr('data-url', desired_url);
    $("#digg-button").attr('data-url', desired_url);
    
}

// Получение необходимого Title
window.saveTitle = (title) => {
    var test_console = title;
    // console.log("title = ", title);
    desired_title = title;

    // VK and Pinterest - image + desc + tittle + description
    $("#vk-button").attr('data-title', meta_ti);
    $("#vk-button").attr('data-description', desired_title);
    $("#vk-button").attr('data-image', meta_img);

    $("#pinterest-button").attr('data-description', desired_title);
    $("#pinterest-button").attr('data-image', meta_img);
    
    // share II tipe
    $("#twitter-button").attr('data-title', desired_title);
    $("#telegram-button").attr('data-title', desired_title);
    $("#whatsapp-button").attr('data-title', desired_title);
    $("#viber-button").attr('data-title', desired_title);
    $("#hackernews-button").attr('data-title', desired_title);
    $("#stumbleupon-button").attr('data-title', desired_title);
    $("#flipboard-button").attr('data-title', desired_title);
    $("#okru-button").attr('data-title', desired_title);

    
    // share III tipe
    $("#tumblr-button").attr('data-title', meta_ti);
    $("#tumblr-button").attr('data-description', desired_title);

    $("#instapaper-button").attr('data-title', meta_ti);
    $("#instapaper-button").attr('data-description', desired_title);

    $("#myspace-button").attr('data-title', meta_ti);
    $("#myspace-button").attr('data-description', desired_title);
    
    $("#blogger-button").attr('data-title', meta_ti);
    $("#blogger-button").attr('data-description', desired_title);

}

// Sharer VK
window.call_share_vk = () => {
    document.querySelector('#vk-button').click();
    // console.log("vkShareBut = ", globalUrlShare);
    gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
};
// Sharer Pinterest
window.call_share_pinterest = () => {
    document.querySelector('#pinterest-button').click();
    // console.log("vkShareBut = ", globalUrlShare);
    gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
};

// share I tipe - button click();

    // Sharer facebook
    window.call_share_facebook = () => {
        document.querySelector('#facebook-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer linkedin
    window.call_share_linkedin = () => {
        document.querySelector('#linkedin-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer reddit
    window.call_share_reddit = () => {
        document.querySelector('#reddit-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer pocket
    window.call_share_pocket = () => {
        document.querySelector('#pocket-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer digg
    window.call_share_digg = () => {
        document.querySelector('#digg-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    
// share II tipe - button click();

    // Sharer Twitter
    window.call_share_twitter = () => {
        document.querySelector('#twitter-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer Telegram
    window.call_share_telegram = () => {
        document.querySelector('#telegram-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer Whatsapp
    window.call_share_whatsapp = () => {
        document.querySelector('#whatsapp-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer viber
    window.call_share_viber = () => {
        document.querySelector('#viber-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer hackernews
    window.call_share_hackernews = () => {
        document.querySelector('#hackernews-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer stumbleupon
    window.call_share_stumbleupon = () => {
        document.querySelector('#stumbleupon-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer flipboard
    window.call_share_flipboard = () => {
        document.querySelector('#flipboard-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

    // Sharer OK.ru
    window.call_share_okru = () => {
        document.querySelector('#okru-button').click();
        // console.log("tgShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };

// share III tipe - button click();

    // Sharer tumblr
    window.call_share_tumblr = () => {
        document.querySelector('#tumblr-button').click();
        // console.log("tumblrShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer instapaper
    window.call_share_instapaper = () => {
        document.querySelector('#instapaper-button').click();
        // console.log("tumblrShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer myspace
    window.call_share_myspace = () => {
        document.querySelector('#myspace-button').click();
        // console.log("tumblrShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };
    // Sharer blogger
    window.call_share_blogger = () => {
        document.querySelector('#blogger-button').click();
        // console.log("tumblrShareBut = ", globalUrlShare);
        gameInstance.SendMessage('_logic', 'getUrl', globalUrlShare);
    };